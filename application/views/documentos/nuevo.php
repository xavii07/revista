<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Agregar</strong> Documento</h1>
        <div class="row w-75 mx-auto">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo site_url('documentos/guardarDocumento') ?>" method="POST">
                            <div class="mb-3">
                                <label class="form-label">Fecha</label>
                                <input type="date" class="form-control" name="fecha_doc" id="fecha_doc" required>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Articulo</label>
                                <select class="form-select" name="id_art" id="id_art" required>
                                    <option value="">--Seleccionar Articulo--</option>
                                    <?php foreach ($articulos as $articulo): ?>
                                        <option value="<?php echo $articulo->id_art; ?>">
                                            <?php echo $articulo->titulo_art; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="mb-3 d-flex justify-content-between gap-2">
                                <div style="flex:1">
                                    <label class="form-label">Editorial</label>
                                    <select class="form-select" name="id_ed" id="id_ed" required>
                                        <option value="">--Seleccionar Editorial--</option>
                                        <?php foreach ($editoriales as $editorial): ?>
                                            <option value="<?php echo $editorial->id_ed; ?>">
                                                <?php echo $editorial->nombre_ed; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div>
                                    <label class="form-label">Respuesta</label>
                                    <select class="form-select" name="id_res" id="id_res" required>
                                        <option value="">--Seleccionar--</option>
                                        <?php foreach ($respuestas as $respuesta): ?>
                                            <option value="<?php echo $respuesta->id_res; ?>">
                                                <?php echo $respuesta->text_res; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Descripcion</label>
                                <textarea type="text" class="form-control" placeholder="Descripción final del documento"
                                    name="descripcion_doc" id="descripcion_doc" rows="3"
                                    required>El comité editorial de la revista VICTEC agradece su participación y le invita a seguir colaborando con nosotros, ya que es grato contar con tan valiosas aportaciones.</textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Guardar</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>