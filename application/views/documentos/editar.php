<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Editar</strong> Documento</h1>
        <div class="row w-75 mx-auto">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo site_url('documentos/actualizarDocumento') ?>" method="POST">
                            <input type="hidden" name="id_doc" value="<?php echo $documentoEditar->id_doc; ?>">
                            <div class="mb-3">
                                <label class="form-label">Fecha</label>
                                <input type="date" class="form-control" name="fecha_doc" id="fecha_doc" required
                                    value="<?php echo $documentoEditar->fecha_doc; ?>">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Articulo</label>
                                <select class="form-select" name="id_art" id="id_art" required>
                                    <option value="">--Seleccionar Articulo--</option>
                                    <?php foreach ($articulos as $articulo): ?>
                                        <option value="<?php echo $articulo->id_art; ?>">
                                            <?php echo $articulo->titulo_art; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="mb-3 d-flex justify-content-between gap-2">
                                <div style="flex:1">
                                    <label class="form-label">Editorial</label>
                                    <select class="form-select" name="id_ed" id="id_ed" required>
                                        <option value="">--Seleccionar Editorial--</option>
                                        <?php foreach ($editoriales as $editorial): ?>
                                            <option value="<?php echo $editorial->id_ed; ?>">
                                                <?php echo $editorial->nombre_ed; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div>
                                    <label class="form-label">Respuesta</label>
                                    <select class="form-select" name="id_res" id="id_res" required>
                                        <option value="">--Seleccionar--</option>
                                        <?php foreach ($respuestas as $respuesta): ?>
                                            <option value="<?php echo $respuesta->id_res; ?>">
                                                <?php echo $respuesta->text_res; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Descripcion</label>
                                <textarea type="text" class="form-control" placeholder="Descripción final del documento"
                                    name="descripcion_doc" id="descripcion_doc" rows="3"
                                    required><?php echo $documentoEditar->descripcion_doc; ?></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Actualizar</button>
                            <a href="<?php echo site_url('documentos/index') ?>" class="btn btn-secondary">Cancelar</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>

<script>
    document.getElementById('id_art').value = "<?php echo $documentoEditar->fkid_art; ?>";
    document.getElementById('id_ed').value = "<?php echo $documentoEditar->fkid_ed; ?>";
    document.getElementById('id_res').value = "<?php echo $documentoEditar->fkid_res; ?>";
</script>