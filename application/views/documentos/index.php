<main class="content">
    <div class="container-fluid p-0">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <h1 class="h3"><strong>Documentos</strong> Recientes</h1>
            <a href="<?php echo site_url('documentos/nuevo') ?>" class="btn btn-primary"> + Agregar</a>
        </div>

        <div class="row">
            <div class="col-12 d-flex">
                <div class="card flex-fill">
                    <?php if ($documentos): ?>
                        <table class="table table-hover my-0">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th class="d-none d-xl-table-cell">Editorial</th>
                                    <th>Articulo</th>
                                    <th class="d-none d-md-table-cell">Respuesta</th>
                                    <th class="d-none d-md-table-cell">Autor</th>
                                    <th class="d-none d-md-table-cell">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($documentos as $documento): ?>
                                    <tr>
                                        <td><?php echo $documento->fecha_doc ?></td>
                                        <td class="d-none d-xl-table-cell"><?php
                                        foreach ($editoriales as $editorial) {
                                            if ($editorial->id_ed == $documento->fkid_ed) {
                                                echo $editorial->nombre_ed;
                                                break;
                                            }
                                        }
                                        ?></td>
                                        <td>
                                            <?php
                                            foreach ($articulos as $articulo) {
                                                if ($articulo->id_art == $documento->fkid_art) {
                                                    echo $articulo->titulo_art;
                                                    break;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <?php
                                            foreach ($respuestas as $respuesta) {
                                                if ($respuesta->id_res == $documento->fkid_res) {
                                                    echo $respuesta->text_res;
                                                    break;
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td class="d-none d-md-table-cell">
                                            <?php
                                            $autoresEncontrados = false;
                                            foreach ($investigaciones as $investigacion) {
                                                if ($investigacion->fkid_ar == $documento->fkid_art) {
                                                    foreach ($autores as $autor) {
                                                        if ($autor->id_au == $investigacion->fkid_au) {
                                                            echo $autor->nombre_au . ' ' . $autor->apellido_au . '<br>';
                                                            $autoresEncontrados = true;
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td class="d-none d-md-table-cell">

                                            <?php
                                            if ($autoresEncontrados) {
                                                ?>
                                                <a href="<?php echo site_url('documentos/editar/' . $documento->id_doc) ?>"
                                                    class="btn btn-sm btn-primary">Editar</a>
                                                <a href="javascript:void(0)"
                                                    onclick="confirmarEliminar('<?php echo site_url('documentos/eliminar/') . $documento->id_doc; ?>', 'Documento');"
                                                    class="btn btn-sm btn-danger">Eliminar</a>
                                                <button class="btn btn-sm btn-success"
                                                    onclick="generarPDF('<?php echo $documento->fkid_ed ?>', '<?php echo $documento->fkid_art ?>', '<?php echo $documento->fkid_res ?>', '<?php echo $documento->id_doc ?>')">PDF</button>
                                                <?php
                                            } else {

                                                ?>
                                                <a href="<?php echo site_url('documentos/editar/' . $documento->id_doc) ?>"
                                                    class="btn btn-sm btn-primary">Editar</a>
                                                <a href="javascript:void(0)"
                                                    onclick="confirmarEliminar('<?php echo site_url('documentos/eliminar/') . $documento->id_doc; ?>', 'Documento');"
                                                    class="btn btn-sm btn-danger">Eliminar</a>
                                                <button class="btn btn-sm btn-success" disabled>PDF</button>
                                                <?php
                                            }
                                            ?>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p>No hay Documentos registrados</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</main>
</div>
</div>

<script>
    function formatFecha(fecha) {
        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        var fechaObjeto = new Date(fecha);

        var dia = fechaObjeto.getUTCDate().toString().padStart(2, "0");
        var mes = meses[fechaObjeto.getUTCMonth()];
        var año = fechaObjeto.getUTCFullYear();

        var fechaFormateada = "Latacunga, " + dia + " de " + mes + " del " + año;

        return fechaFormateada;
    }

    function generarPDF(fkid_ed, fkid_art, fkid_res, id_doc) {
        //filtar documento en documentos
        var fechaDocumento = '';
        var descripcion = '';
        var idRespuesta;
        <?php foreach ($documentos as $documento): ?>
            if (<?php echo $documento->id_doc ?> == id_doc) {
                fechaDocumento = '<?php echo $documento->fecha_doc ?>';
                descripcion = '<?php echo $documento->descripcion_doc ?>';
                idRespuesta = '<?php echo $documento->fkid_res ?>';
            }
        <?php endforeach; ?>
        //filtrar el editorial de editoriales
        var editorial = '';
        var director = '';
        var firma = '';
        <?php foreach ($editoriales as $editorial): ?>
            if (<?php echo $editorial->id_ed ?> == fkid_ed) {
                editorial = '<?php echo $editorial->nombre_ed ?>';
                director = '<?php echo $editorial->director_ed ?>';
                firma = '<?php echo $editorial->firma_ed ?>';
            }
        <?php endforeach; ?>

        //filtar respuesta en respuestas
        var respuesta = '';
        <?php foreach ($respuestas as $res): ?>
            if (<?php echo $res->id_res ?> == fkid_res) {
                respuesta = '<?php echo $res->respuesta_res ?>';
            }
        <?php endforeach; ?>

        //filtrar articulo en articulos
        var articulo = '';
        var url = '';
        <?php foreach ($articulos as $articulo): ?>
            if (<?php echo $articulo->id_art ?> == fkid_art) {
                articulo = '<?php echo $articulo->titulo_art ?>';
                url = '<?php echo $articulo->url ?>';
            }
        <?php endforeach; ?>

        //filtrar Autores en investigaciones con id_art
        var autoresL = [];
        <?php foreach ($investigaciones as $investigacion): ?>
            if (<?php echo $investigacion->fkid_ar ?> == fkid_art) {
                <?php foreach ($autores as $autor): ?>
                    <?php if ($autor->id_au == $investigacion->fkid_au): ?>
                        autoresL.push('<?php echo $autor->nombre_au . ' ' . $autor->apellido_au ?>');
                    <?php endif; ?>
                <?php endforeach; ?>
            }
        <?php endforeach; ?>


        var aceptado = idRespuesta == 1
        console.log(aceptado)





        var docDefinition = {
            content: [
                { text: 'REVISTA ' + editorial.toLocaleUpperCase(), alignment: 'right', fontSize: 20, color: "#00205b", bold: true },
                { text: formatFecha(fechaDocumento), alignment: 'right', marginTop: 20 },

                //bucle para listar autoresL
                {
                    text: autoresL.join('\n'),
                    marginTop: 10,
                    lineHeight: 2,
                    bold: true,
                    listStyleType: 'none'
                },
                { text: 'Presentes', marginTop: 10, bold: true },
                { text: respuesta, marginTop: 50, lineHeight: 1.5 },
                { text: '"' + articulo + '"', marginTop: 20, bold: true, fontSize: 15, alignment: 'center', lineHeight: 2 },
                { text: 'Mismo que cumple con los lineamientos estipulados para la publicación.', lineHeight: 1.5, marginTop: 10, fontSize: aceptado ? 12 : 0 },
                {
                    text: 'Su artículo es presentado en forma digital y formato PDF que se incluye en el volúmen II, número II de nuestra Revista con ISSN: 2737-6214 y dirección electrónica: ',
                    lineHeight: 1.5, marginTop: 10, fontSize: aceptado ? 12 : 0,
                },
                { text: url, link: url, color: '#0099cc', decoration: 'underline', fontSize: aceptado ? 12 : 0 },
                { text: descripcion, marginTop: 30, lineHeight: 1.5 },
                { text: 'Sin otro particular, le saluda', marginTop: 20, bold: true, alignment: 'center' },
                { text: 'ATENTAMENTE', marginTop: 5, bold: true, alignment: 'center' },
                { qr: 'text in QR', alignment: 'center', marginTop: 30, fit: '100', version: 7 },
                { text: director, marginTop: 20, alignment: 'center' },
                { text: 'Director Editorial', marginTop: 5, alignment: 'center' },
            ]
        };

        pdfMake.createPdf(docDefinition).print();


    }
</script>