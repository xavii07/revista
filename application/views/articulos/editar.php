<main class="content">
  <div class="container-fluid p-0">
    <h1>EDITAR ARTÍCULOS</h1>
    <form class="" action="<?php echo site_url('articulos/actualizarArticulo') ?>" method="post" id="frm_nuevo_articulo"
      enctype="multipart/form-data">
      <input type="hidden" name="id_art" id="id_art" value="<?php echo $articuloEditar->id_art; ?>">
      <label for=""> <b>TÍTULO:</b> </label>
      <input type="text" name="titulo_art" id="titulo_art" value="<?php echo $articuloEditar->titulo_art; ?>"
        class="form-control" placeholder="Ingrese el Título del Artículo" required>
      <br>
      <label for=""> <b>URLs:</b> </label>
      <input type="text" name="url" id="url" value="<?php echo $articuloEditar->url; ?>" class="form-control"
        placeholder="Ingrese la Dirección Electrónica" required>
      <br>
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp
            Actualizar</button> &nbsp &nbsp
          <a href="<?php echo site_url('articulos/index'); ?>" class="btn btn-danger"> <i
              class="fa fa-xmark"></i> &nbsp Cancelar</a>
        </div>
      </div>
    </form>
    <br>
    <br>

    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_articulo").validate({
          rules: {
            "titulo_art": {
              required: true,
              lettersOnly: true
            },
            "url": {
              required: true
            }
          },
          messages: {
            "titulo_art": {
              required: "Debe ingresar el Título del Atículo",
              lettersOnly: "Ingrese solo letras"
            },
            "url": {
              required: "Debe ingresar la Dirección Electrónica"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>

  </div>
</main>
