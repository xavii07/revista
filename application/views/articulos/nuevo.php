<main class="content">
  <div class="container-fluid p-0">
    <h2>
      <b>
        <i class="fa fa-plus-circle"></i>
        NUEVO ARTÍCULO
      </b>
    </h2>
    <br>
    <form class="" action="<?php echo site_url('articulos/guardarArticulo') ?>" method="post"
      enctype="multipart/form-data" id="frm_nuevo_articulo" onsubmit="return validarFormulario()">
      <label for=""> <b>TÍTULO:</b> </label>
      <input type="text" name="titulo_art" id="titulo_art" value="" class="form-control"
        placeholder="Ingrese el nombre del Atículo" required>
      <br>
      <label for=""> <b>URLs:</b> </label>
      <input type="text" name="url" id="url" value="" class="form-control"
        placeholder="Ingrese la Dirección Electrónica" required>
      <br>
      <!--Botones de guardar y cancelar -->
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"><i class="fa-regular fa-floppy-disk"></i>
            Guardar</button>
          <a href="<?php echo site_url("articulos/index") ?>" class="btn btn-danger"><i
              class="fa-solid fa-xmark"></i> Cancelar</a>
        </div>
      </div>
    </form>

    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_articulo").validate({
          rules: {
            "titulo_art": {
              required: true
            },
            "url": {
              required: true
            }
          },
          messages: {
            "titulo_art": {
              required: "Debe ingresar el Título del Atículo"
            },
            "url": {
              required: "Debe ingresar la Dirección Electrónica"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>

  </div>
</main>
