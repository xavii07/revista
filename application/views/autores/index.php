<main class="content">
  <div class="container-fluid p-0">

    <h1><i class="mdi mdi-bank menu-icon"></i> Autores</h1><br>
    <div class="row">
      <div class="col-md-12 text-end">
        <a href="<?php echo site_url('autores/nuevo') ?>" class="btn btn-outline-success"> <i
            class="fa fa-plus-circle"></i> Agregar Autores</a>
      </div>
    </div>
    <?php if ($listadoAutores): ?>
      <div class="table-responsive pt-3">
        <table class="table table-bordered" id="tbl_autores">
          <thead>
            <tr class="table-info">
              <th>ID</th>
              <th>NOMBRE</th>
              <th>APELLIDO</th>
              <th>ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoAutores as $autor): ?>
              <tr class="">
                <td><?php echo $autor->id_au; ?></td>
                <td><?php echo $autor->nombre_au; ?></td>
                <td><?php echo $autor->apellido_au; ?></td>
                <td>
                  <a href="<?php echo site_url('autores/editar/') . $autor->id_au; ?>" class="btn btn-warning" title="Editar">
                    <i class="fa fa-pen"></i> </a>
                  <a class="btn btn-danger delete-btn" href="<?php echo site_url('autores/borrar/') . $autor->id_au; ?>" title="Eliminar" data-id_au="<?php echo $autor->id_au; ?>"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    <?php else: ?>
      <div class="alert alert-danger">
        NO SE ENCONTRARON AUTORES REGISTRADOS
      </div>
    <?php endif; ?>
    <script type="text/javascript">
            $(document).ready(function() {
                $('#tbl_autores').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa-solid fa-file-pdf"></i> Exportar a PDF',
                            className: 'btn btn-outline-info',
                            messageTop: 'REPORTE DE AUTORES ',
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa-solid fa-print"></i> Imprimir',
                            className: 'btn btn-outline-info',
                            messageTop: 'REPORTE DE AUTORES ',
                        },
                        {
                            extend: 'csv',
                            text: '<i class="fa-solid fa-file-csv"></i> Exportar a CSV',
                            className: 'btn btn-outline-info',
                            messageTop: 'REPORTE DE AUTORES ',
                        }
                    ],
                    language: {
                        url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                    }
                } );
            } );
        </script>

  </div>
</main>
<script>
$(document).ready(function() {
    $('.delete-btn').click(function(event) {
        // Evitar el comportamiento predeterminado del enlace
        event.preventDefault();

        var id = $(this).data('id_au');
        Swal.fire({
            title: '¿Está seguro de eliminar este registro?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Sí',
            denyButtonText: 'No',
            customClass: {
                actions: 'my-actions',
                cancelButton: 'order-1 right-gap',
                confirmButton: 'order-2',
                denyButton: 'order-3',
            },
        }).then((result) => {
            if (result.isConfirmed) {
                // Realizar la acción de eliminación
                // Por ejemplo, redireccionar a una URL que maneje la eliminación
                window.location.href = "<?php echo site_url('autores/borrar/'); ?>" + id;
                // No redirigir aquí, dejar que el servidor maneje la redirección después de eliminar
            } else if (result.isDenied) {
                // No hacer nada si el usuario cancela la eliminación
            }
        });
    });
});

</script>
