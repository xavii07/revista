<main class="content">
  <div class="container-fluid p-0">
    <h1>EDITAR AUTORES</h1>
    <form class="" id="frm_nuevo_autor" action="<?php echo site_url('autores/actualizarAutor') ?>" method="post"
      enctype="multipart/form-data">
      <input type="hidden" name="id_au" id="id_au" value="<?php echo $autorEditar->id_au; ?>">
      <label for=""> <b>NOMBRE:</b> </label>
      <input type="text" name="nombre_au" id="nombre_au" value="<?php echo $autorEditar->nombre_au; ?>"
        class="form-control" placeholder="Ingrese el Nombre del Autor" required>
      <br>
      <label for=""> <b>APELLIDO:</b> </label>
      <input type="text" name="apellido_au" id="apellido_au" value="<?php echo $autorEditar->apellido_au; ?>"
        class="form-control" placeholder="Ingrese el Apellido del Autor" required>
      <br>
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp
            Actualizar</button> &nbsp &nbsp
          <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark"></i>
            &nbsp Cancelar</a>
        </div>
      </div>
    </form>
    <br>
    <br>
    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_autor").validate({
          rules: {
            "nombre_au": {
              required: true,
              lettersOnly: true
            },
            "apellido_au": {
              required: true,
              lettersOnly: true
            }
          },
          messages: {
            "nombre_au": {
              required: "Debe ingresar el nombre del Autor",
              lettersOnly: "Ingrese solo letras"
            },
            "apellido_au": {
              required: "Debe ingresar el apellido del Autor"
          lettersOnly: "Ingrese solo letras"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_autor").validate({
          rules: {
            "nombre_au": {
              required: true,
              lettersOnly: true
            },
            "apellido_au": {
              required: true,
              lettersOnly: true
            }
          },
          messages: {
            "nombre_au": {
              required: "Debe ingresar el Nombre del Autor",
              lettersOnly: "Ingrese solo letras"
            },
            "apellido_au": {
              required: "Debe ingresar el Apellido del Autor",
              lettersOnly: "Ingrese solo letras"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>
  </div>
</main>
