<main class="content">
  <div class="container-fluid p-0">
    <h2>
      <b>
        <i class="fa fa-plus-circle"></i>
        NUEVO AUTOR
      </b>
    </h2>
    <br>
    <form class="" id="frm_nuevo_autor" action="<?php echo site_url('autores/guardarAutor') ?>" method="post"
      enctype="multipart/form-data">
      <label for=""> <b>NOMBRE:</b> </label>
      <input type="text" name="nombre_au" id="nombre_au" value="" class="form-control"
        placeholder="Ingrese el Nombre del autor" required>
      <br>
      <label for=""> <b>APELLIDO:</b> </label>
      <input type="text" name="apellido_au" id="apellido_au" value="" class="form-control"
        placeholder="Ingrese el Apellido del Autor" required>
      <br>
      <!--Botones de guardar y cancelar -->
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"><i class="fa-regular fa-floppy-disk"></i>
            Guardar</button>
          <a href="<?php echo site_url("autores/index") ?>" class="btn btn-danger"><i
              class="fa-solid fa-xmark"></i> Cancelar</a>
        </div>
      </div>
    </form>
    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_autor").validate({
          rules: {
            "nombre_au": {
              required: true,
              lettersOnly: true
            },
            "apellido_au": {
              required: true,
              lettersOnly: true
            }
          },
          messages: {
            "nombre_au": {
              required: "Debe ingresar el Nombre del Autor",
              lettersOnly: "Ingrese solo letras"
            },
            "apellido_au": {
              required: "Debe ingresar el Apellido del Autor",
              lettersOnly: "Ingrese solo letras"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>
  </div>

</main>
