<?php

class Documentos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Documento");
        $this->load->model("Respuesta");
        $this->load->model("Articulo");
        $this->load->model("Editorial");
        $this->load->model("Investigacion");
        $this->load->model("Autor");
    }

    public function index()
    {
        $data["documentos"] = $this->Documento->consultarTodos();
        $data["editoriales"] = $this->Editorial->consultarTodos();
        $data["articulos"] = $this->Articulo->consultarTodos();
        $data["respuestas"] = $this->Respuesta->consultarTodos();
        $data["investigaciones"] = $this->Investigacion->consultarTodos();
        $data["autores"] = $this->Autor->consultarTodos();

        $this->load->view("header");
        $this->load->view('documentos/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $data["respuestas"] = $this->Respuesta->consultarTodos();
        $data["articulos"] = $this->Articulo->consultarTodos();
        $data["editoriales"] = $this->Editorial->consultarTodos();
        $this->load->view("header");
        $this->load->view('documentos/nuevo', $data);
        $this->load->view('footer');
    }

    public function editar($id)
    {
        $data["documentoEditar"] = $this->Documento->obtenerPorId($id);
        $data["respuestas"] = $this->Respuesta->consultarTodos();
        $data["articulos"] = $this->Articulo->consultarTodos();
        $data["editoriales"] = $this->Editorial->consultarTodos();

        $this->load->view("header");
        $this->load->view('documentos/editar', $data);
        $this->load->view('footer');
    }

    public function eliminar($id)
    {
        $this->Documento->eliminar($id);
        $this->session->set_flashdata('mensaje', 'El Documento fue eliminado correctamente');
        redirect('documentos/index');
    }

    public function guardarDocumento()
    {
        $datosNuevoDocumento = array(
            "fecha_doc" => $this->input->post("fecha_doc"),
            "descripcion_doc" => $this->input->post("descripcion_doc"),
            #foreing key editorial
            "fkid_ed" => $this->input->post("id_ed"),
            #foreing key articulo
            "fkid_art" => $this->input->post("id_art"),
            #foreing key respuesta
            "fkid_res" => $this->input->post("id_res"),
        );

        $this->Documento->insertar($datosNuevoDocumento);
        $this->session->set_flashdata('mensaje', 'El Documento fue registrado correctamente');
        redirect("documentos/index");
    }

    public function actualizarDocumento()
    {
        $id_doc = $this->input->post("id_doc");

        //$documentoEditar = $this->Documento->obtenerPorId($id_doc);

        // Datos del documento a actualizar
        $datosDocumento = array(
            "fecha_doc" => $this->input->post("fecha_doc"),
            "descripcion_doc" => $this->input->post("descripcion_doc"),
            #foreing key editorial
            "fkid_ed" => $this->input->post("id_ed"),
            #foreing key articulo
            "fkid_art" => $this->input->post("id_art"),
            #foreing key respuesta
            "fkid_res" => $this->input->post("id_res"),
        );

        // Actualizar el documento
        $this->Documento->actualizar($id_doc, $datosDocumento);
        $this->session->set_flashdata('mensaje', 'El Documento fue actualizado correctamente');
        redirect("documentos/index");
    }
}
