<?php
class Autores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Autor");
    }

    public function index()
    {
        $data["listadoAutores"] = $this->Autor->consultarTodos();
        $this->load->view("header");
        $this->load->view("autores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id)
    {
        $this->Autor->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Autor Eliminado Exitosamente");
        redirect("autores/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("autores/nuevo");
        $this->load->view("footer");
    }

    //capturando datos y e insertando
    public function guardarAutor(){
      $datosNuevoAutor=array(
      "nombre_au"=>$this->input->post("nombre_au"),
      "apellido_au"=>$this->input->post("apellido_au"));
      $this->Autor->insertar($datosNuevoAutor);
      $this->session->set_flashdata("confirmacion","Autor Guardado Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se agrego al Autor: </h5>".$datosNuevoAutor['nombre_au']);
      redirect('autores/index');
    }
    //Renderizar el formulario de editar
    public function editar($id){
      $data["autorEditar"]=$this->Autor->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("autores/editar",$data);
      $this->load->view("footer");
    }
    //actualizar
    public function actualizarAutor(){
      $id_au=$this->input->post("id_au");
      $datosAutor=array(
        "nombre_au"=>$this->input->post("nombre_au"),
        "apellido_au"=>$this->input->post("apellido_au")
      );
      $this->Autor->actualizar($id_au,$datosAutor);
      $this->session->set_flashdata("confirmacion",
      "Autor actualizado exitosamente");
      redirect('autores/index');
    }
  }//fin de la clase
 ?>
