<?php
  class Articulos extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Articulo");
      //desabilitando errores y advertencias
      //error_reporting(0);
    }//fin de la funcionconstructor
    //renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoArticulos"]=$this->Articulo->consultarTodos();//array asociativo:data, tiene una posicion: listadoHospitales
      $this->load->view("header");
      $this->load->view("articulos/index",$data);
      $this->load->view("footer");
    }//fin de la funcion index
    //eliminacion de articulo recibiendo el id por get
    public function borrar($id)
    {
        $this->Articulo->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Articulo Eliminado Exitosamente");
        redirect("articulos/index");
    }//fin de la funcion borrar
    //renderizacion de formulario nuevo articulo
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("articulos/nuevo");
      $this->load->view("footer");
    }
    //capturando datos y e insertando en Articulo
    public function guardarArticulo(){
      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/

      $datosNuevoArticulo=array(
      "titulo_art"=>$this->input->post("titulo_art"),
      "url"=>$this->input->post("url"));
      $this->Articulo->insertar($datosNuevoArticulo);
      $this->session->set_flashdata("confirmacion","Articulo Guardado Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se creo el Articulo: </h5>".$datosNuevoArticulo['titulo_art']);
      redirect('articulos/index');
    }
    //Renderizar el formulario de edicion
    public function editar($id){
      $data["articuloEditar"]=$this->Articulo->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("articulos/editar",$data);
      $this->load->view("footer");
    }
    //actualizar Articulo
    public function actualizarArticulo(){
      $id_art=$this->input->post("id_art");
      $datosArticulo=array(
        "titulo_art"=>$this->input->post("titulo_art"),
        "url"=>$this->input->post("url")
      );
      $this->Articulo->actualizar($id_art,$datosArticulo);
      $this->session->set_flashdata("confirmacion",
      "Articulo actualizado exitosamente");
      redirect('articulos/index');
    }
  }//fin de la clase
 ?>
