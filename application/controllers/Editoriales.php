<?php
    error_reporting(0);
  
  class Editoriales extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Editorial");


    }

    public function index(){
      $data["listadoEditorial"]=$this->Editorial->consultarTodos();
      $this->load->view('header');
      $this->load->view('editoriales/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de editoriales con get ID
    public function borrar($id){
      $this->Editorial->eliminar($id);
      $this->session->set_flashdata("confirmacion","Editorial eliminado exitosamente");
      redirect("editoriales/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoEditoriales"]=$this->Editorial->consultarTodos();
      $this->load->view("header");
      $this->load->view("editoriales/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarEditorial(){
      
      $config['upload_path']=APPPATH.'../uploads/Firmas/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="Editorial_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("firma_ed")){ //intentando subir el archivo
          $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
          $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        }else{
          $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
        }

      $datosNuevoEditorial=array(

        "id_ed"=>$this->input->post("id_ed"),
        "nombre_ed"=>$this->input->post("nombre_ed"),
        "correo_ed"=>$this->input->post("correo_ed"),
        "telefono_ed"=>$this->input->post("telefono_ed"),
        "director_ed"=>$this->input->post("director_ed"),
        "firma_ed"=>$nombre_archivo_subido

      );
      $this->Editorial->insertar($datosNuevoEditorial);
      $this->session->set_flashdata("confirmacion","Editorial guardado exitosamente");
      redirect('editoriales/index');

    }


    //editar
    public function editar($id_ed){
      $data["editorialEditar"]=
      $this->Editorial->obtenerPorId($id_ed);
      $this->load->view('header');
      $this->load->view('editoriales/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarEditorial(){
      //INICIO PROCESO DE SUBIDA DE ARCHIVO/
      $config['upload_path']=APPPATH.'../uploads/Firmas/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio = "Editorial_" . time() * rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("firma_ed")){ //intentando subir el archivo
          $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
          $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        }else{
          $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
        }

      $id=$this->input->post("id_ed");
      $datosEditorial=array(
        "id_ed"=>$this->input->post("id_ed"),
        "nombre_ed"=>$this->input->post("nombre_ed"),
        "correo_ed"=>$this->input->post("correo_ed"),
        "telefono_ed"=>$this->input->post("telefono_ed"),
        "director_ed"=>$this->input->post("director_ed"),
        "firma_ed"=>$nombre_archivo_subido

      );
      $this->Editorial->actualizar($id, $datosEditorial);
      $this->session->set_flashdata("confirmacion","Editorial actualizado exitosamente");
      redirect('editoriales/index');
    }




  }// fin de clase

