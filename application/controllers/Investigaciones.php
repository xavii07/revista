<?php
class Investigaciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Investigacion");
        $this->load->model("Articulo");
        $this->load->model("Autor");
    }

    public function index()
    {
        $data["investigaciones"] = $this->Investigacion->consultarTodos();
        $data["articulos"] = $this->Articulo->consultarTodos();
        $data["autores"] = $this->Autor->consultarTodos();
        $this->load->view("header");
        $this->load->view("investigaciones/index", $data);
        $this->load->view("footer");
    }

    public function eliminar($id)
    {
        $this->Investigacion->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Investigación Eliminada Exitosamente");
        redirect("investigaciones/index");
    }

    public function nuevo()
    {
        $data["articulos"] = $this->Articulo->consultarTodos();
        $data["autores"] = $this->Autor->consultarTodos();
        $this->load->view("header");
        $this->load->view("investigaciones/nuevo", $data);
        $this->load->view("footer");
    }

    //capturando datos y e insertando
    public function guardarInvestigacion(){
      $datosNuevoInvestigacion=array(
      "fkid_au"=>$this->input->post("id_au"),
      "fkid_ar"=>$this->input->post("id_art"));
      $this->Investigacion->insertar($datosNuevoInvestigacion);
      $this->session->set_flashdata("confirmacion","Investigación Guardada Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se agrego la Investigación: </h5>".$datosNuevoInvestigacion['fkid_ar']);
      redirect('investigaciones/index');
    }
    //Renderizar el formulario de editar
    public function editar($id){
      $data["investigacionEditar"]=$this->Investigacion->obtenerPorId($id);
      $data["articulos"] = $this->Articulo->consultarTodos();
      $data["autores"] = $this->Autor->consultarTodos();
      $this->load->view("header");
      $this->load->view("investigaciones/editar",$data);
      $this->load->view("footer");
    }

    //actualizar
    public function actualizarInvestigacion(){
      $id_in=$this->input->post("id_in");
      $datosInvestigacion=array(
        "fkid_ar"=>$this->input->post("id_art"),
        "fkid_au"=>$this->input->post("id_au")
      );
      $this->Investigacion->actualizar($id_in,$datosInvestigacion);
      $this->session->set_flashdata("confirmacion",
      "Investigación actualizada exitosamente");
      redirect('investigaciones/index');
    }
  }//fin de la clase
 ?>
