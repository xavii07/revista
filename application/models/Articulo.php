<?php
  /**
   *
   */
  class Articulo extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }//fin de la funcion constructor
    //insertar nuevos
    function insertar($datos){//$datos: es un array- conjunto de datos
      $respuesta=$this->db->insert("Articulo",$datos);//insert nos deja insertar registros permite dos parametros: la tabla y un conjunto de datos informativos como nombre, etc
      return $respuesta;
    }//fin funcion insertar
    //consulta de datos
    function consultarTodos(){
      $articulos=$this->db->get("Articulo");
      if($articulos->num_rows()>0) {
        return $articulos->result();
      } else {
        return false;
      }//fin del else
    }//fin de la funcion consulta
    //Eliminacion  por id
    function eliminar($id){
      $this->db->where("id_art",$id);
      return $this->db->delete("Articulo");
    }//fin de la funcion eliminar
    //Consulta de un solo articulo
    function obtenerPorId($id){
      $this->db->where("id_art",$id);
      $articulo=$this->db->get("Articulo");
      if ($articulo->num_rows()>0) {
        return $articulo->row();
      } else {
        return false;
      }
    }
    //actualizar HOSPITAL
    function actualizar($id,$datos){
      $this->db->where("id_art",$id);
      return $this->db->update("Articulo",$datos);
    }
  }//fin de la clase Hospital
 ?>
