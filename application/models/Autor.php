<?php
  /**
   *
   */
  class Autor extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }//fin de la funcion constructor
    //insertar nuevo
    function insertar($datos){//$datos: es un array- conjunto de datos
      $respuesta=$this->db->insert("Autor",$datos);//insert nos deja insertar registros permite dos parametros: la tabla y un conjunto de datos informativos como nombre, etc
      return $respuesta;
    }//fin funcion insertar
    //consulta de datos
    function consultarTodos(){
      $autores=$this->db->get("Autor");
      if($autores->num_rows()>0) {
        return $autores->result();
      } else {
        return false;
      }//fin del else
    }//fin de la funcion consulta
    //Eliminacion por id
    function eliminar($id){
      $this->db->where("id_au",$id);
      return $this->db->delete("Autor");
    }//fin de la funcion eliminar
    //eliminar datos
    //Consulta de uno
    function obtenerPorId($id){
      $this->db->where("id_au",$id);
      $autor=$this->db->get("Autor");
      if ($autor->num_rows()>0) {
        return $autor->row();
      } else {
        return false;
      }
    }
    //actualizar
    function actualizar($id,$datos){
      $this->db->where("id_au",$id);
      return $this->db->update("Autor",$datos);
    }
  }//fin de la clase
 ?>
