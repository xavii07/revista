<?php

class Documento extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert('Documento', $datos);
    }

    function consultarTodos()
    {
        $documentos = $this->db->get('Documento');
        if ($documentos->num_rows() > 0) {
            return $documentos->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_doc', $id);
        $documento = $this->db->get('Documento');
        if ($documento->num_rows() > 0) {
            return $documento->row();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {

        try {
            $this->db->where('id_doc', $id);
            return $this->db->delete('Documento');
        } catch (\Throwable $th) {
            return false;
        }

    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_doc', $id);
        return $this->db->update('Documento', $datos);
    }
}
